#!/bin/bash

## Genbank
curl --output-dir ./ -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/024/363/595/GCA_024363595.1_UCB_Xborealis_1/GCA_024363595.1_UCB_Xborealis_1_assembly_report.txt
curl --output-dir ./raw -O https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/024/363/595/GCA_024363595.1_UCB_Xborealis_1/GCA_024363595.1_UCB_Xborealis_1_genomic.fna.gz
